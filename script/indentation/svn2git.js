var katescript = {
    "name": "svn2git",
    "author": "Nicolás Alvarez",
    "license": "BSD License",
    "revision": 1,
    "kate-version": "5.0",
    "indent-languages": ["svn2git rules"],
    "priority": 0
};

triggerCharacters='hy'; // for end matcH and end repositorY

function indent(line, indentWidth, ch)
{
    if (ch == 'y' && document.startsWith(line, "end repository", true)) {
        return 0;
    }
    else if (ch == 'h' && document.startsWith(line, "end match", true)) {
        return 0;
    }
    else if (document.startsWith(line-1, "create repository", true)) {
        return indentWidth;
    }
    else if (document.startsWith(line-1, "match", true)) {
        return indentWidth;
    }

    if (ch == '\n')
        return -1;
    else
        return -2;
}
